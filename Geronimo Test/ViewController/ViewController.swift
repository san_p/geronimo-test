//
//  ViewController.swift
//  Geronimo Test
//
//  Created by Prescillia on 01/08/2018.
//  Copyright © 2018 Prescillia. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Alamofire

class ViewController: UIViewController {

    // IBOutlet
    @IBOutlet weak var countryTableView: UITableView!
   
    // Variables
    var countries: BehaviorRelay<[Country]> = BehaviorRelay(value: [])
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request("https://restcountries.eu/rest/v2/all").responseJSON { response in
            if let result = response.result.value {
                let jsonDict = result as? NSDictionary
                
                print(jsonDict)
            }

        }
        
        initTableView()
    }
    
    func initTableView() {
        countries.bind(to: countryTableView.rx.items(cellIdentifier: "coutryCell", cellType: CountryTableViewCell.self)) { (row, item, cell) in
            cell.countryLabel.text = item.name
            cell.capitalLabel.text = item.capital
            cell.flagImageView.image = item.flag
        }.disposed(by: disposeBag)
    }
}

