//
//  Country.swift
//  Geronimo Test
//
//  Created by Prescillia on 01/08/2018.
//  Copyright © 2018 Prescillia. All rights reserved.
//

import Foundation
import UIKit

class Country {
    let name: String?
    let capital: String?
    let flag: UIImage?
    let population: Int?
    let currency: String?
    let borders: [Country]?
    
    init(name: String, capital: String, flag: UIImage, population: Int, currency: String, borders: [Country]) {
        self.name = name
        self.capital = capital
        self.flag = flag
        self.population = population
        self.currency = currency
        self.borders = borders
    }
}
